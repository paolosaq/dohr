<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?php
session_start();
if(!isset($_SESSION["Username"])) {
	$condotta = 0;
}
else {
	$condotta = 1;
}

/// CONTROLLO SERVER OFFLINE ///
if (!@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)){
	session_destroy();
	$condotta = 0;
}

include("includes/config.php");
include("gestorelingua.php");

$check = 0; 
$count = 0;
$quantity = $_POST["quantity"];
$gold=0;
$units=0;
$item=$_POST["item"];

if ($_POST['action'] == 'gold') {
	$gold=($_POST["gold"]*$quantity);
} else if ($_POST['action'] == 'units') {
    $units=($_POST["units"]*$quantity);
} else {
    //invalid action!
}

$changebackgroundred = <<< EOD
<div>
<script>
document.body.style.backgroundImage = 'url("backdrariva.jpg")';
</script>
</div>
EOD;

?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style/style.css">
		<meta name="viewport" content="width-device-width, initial scale=1"/>
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
		<meta name="keywords" content="<?php echo $CMS_SERVER_KEYWORDS; ?>"/>
		<title><?php echo $CMS_SERVER_NAME;?></title>
	</head>
	<body>
		
		<?php 
		if($_SESSION["Faction"] == "Providentia") {
			$style="_blue";
		} 
		else if($_SESSION["Faction"] == "Drariva") {
			$style="_red";
			echo $changebackgroundred;
		}
		?>
		
		<div id="main">
			<div id="wrapper">
				<div id="header">
					<div id="LogoIMG<?php echo $style?>"></div>
				</div><!--Header-->
				<div id="body_sopra"></div><!--Body sopra-->
				<div id="body_centro_confirm">
					<div id="Content">
						<div id="sopra<?php echo $style?>"></div>
						<div id="centro<?php echo $style?>">
						
							<?php
							if($condotta == 0) {
								echo "<br><div class='loading".$style."'></div><br><br><br><br>
								<meta http-equiv='refresh' content='3; URL=shop?lingua=".$lingua."'>";
							}
							else {
								
								if($quantity==0) {
									echo $noquantity;
									echo "<br><div class='loading".$style."'></div><br><br><br><br>
									<meta http-equiv='refresh' content='3; URL=shop?lingua=".$lingua."'>";
								}
								else if($gold!=0) {
									$gold_control = $conn->prepare("SELECT gold FROM accounts WHERE szUserID='".$_SESSION["Username"]."'");
									$gold_control->execute();
									
									while($row = $gold_control->fetch(PDO::FETCH_ASSOC)) {
										$goldcontrolled = $row["gold"];
									}
									
									if($goldcontrolled<$gold) {
										echo $notenoughgold;
										echo "<br><div class='loading".$style."'></div><br><br><br><br>
										<meta http-equiv='refresh' content='3; URL=shop?lingua=".$lingua."'>";
									}
									else {
										$write = $conn->prepare("UPDATE accounts SET gold=gold-'".$gold."' , ".$item."=".$item."+".$quantity." WHERE szUserID='".$_SESSION["Username"]."'");
										$write->execute();
										
										if($write) {
											echo $buysuccess;
											echo "<br><div class='loading".$style."'></div><br><br><br><br>
											<meta http-equiv='refresh' content='3; URL=shop?lingua=".$lingua."'>";
										}
										else {
											echo $buyfailure;
											echo "<br><div class='loading".$style."'></div><br><br><br><br>
											<meta http-equiv='refresh' content='3; URL=shop?lingua=".$lingua."'>";
										}
									}
								}
								
								else if($units!=0) {
									$units_control = $conn->prepare("SELECT units FROM accounts WHERE szUserID='".$_SESSION["Username"]."'");
									$units_control->execute();
									
									while ($row = $units_control->fetch(PDO::FETCH_ASSOC)) {
										$unitscontrolled = $row["units"];
									}
									
									if($unitscontrolled<$units) {
										echo $notenoughunits;
										echo "<br><div class='loading".$style."'></div><br><br><br><br>
										<meta http-equiv='refresh' content='3; URL=shop?lingua=".$lingua."'>";
									}
									else {
										$write = $conn->prepare("UPDATE accounts SET units=units-'".$units."' , ".$item."=".$item."+".$quantity." WHERE szUserID='".$_SESSION["Username"]."'");
										$write->execute();
										
										if($write) {
											echo $buysuccess;
											echo "<br><div class='loading".$style."'></div><br><br><br><br>
											<meta http-equiv='refresh' content='3; URL=shop?lingua=".$lingua."'>";
										}
										else {
											echo $buyfailure;
											echo "<br><div class='loading".$style."'></div><br><br><br><br>
											<meta http-equiv='refresh' content='3; URL=shop?lingua=".$lingua."'>";
										}
									}
								}
							}
							?>
							
						</div><!--Centro-->
						<div id="sotto<?php echo $style?>"></div>
					</div>
					
					<!--Distanza-->
					<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
				
				</div><!--Body centro-->
				<div id="body_sotto"></div><!--Body sotto-->
			</div> <!--wrapper-->
		</div> <!--main-->
	</body>
</html>