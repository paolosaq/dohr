<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?php
session_start();
if(!isset($_SESSION["Username"])){
	$condotta = 0;
} else {
	$condotta = 1;
}

/// CONTROLLO SERVER OFFLINE ///
if (!@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)){
	session_destroy();
	$condotta = 0;
}

include("includes/time.php");
include("includes/config.php");
include("gestorelingua.php");

$check = 0; 
$count = 0;
$style="_blue";
$ok=false;

$Username = $_POST['UsernameS'];
$Password = $_POST['PasswordS'];
$PasswordMD5 = md5($_POST['PasswordS']);

$changebackgroundred = <<< EOD
<div>
<script>
document.body.style.backgroundImage = 'url("backdrariva.jpg")';
</script>
</div>
EOD;

?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style/style.css">
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
		<meta name="keywords" content="<?php echo $CMS_SERVER_KEYWORDS; ?>"/>
		<title><?php echo $CMS_SERVER_NAME;?></title>
	</head>
	<body>
		<?php
		/// REDIRECT IF SERVER IS OFFLINE ///
		if (!@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)){
			echo "<script>window.location = 'forum_index?lingua=".$lingua."'</script>";
		}

		if($condotta == 1 && $_SESSION["Faction"] == "Providentia"){
			$style="_blue";
		} 
		else if($condotta == 1 && $_SESSION["Faction"] == "Drariva"){
			$style="_red";
			echo $changebackgroundred;
		} 
		?>

		<div id="main">
			<div id="wrapper">			
				<div id="header">		
					<div id="LogoIMG<?php echo $style?>"></div>		
				</div><!--Header-->	
				<div id="body_sopra"></div>		
				<div id="body_centro_confirm">		
					<div id="Content">			
						<div id="sopra<?php echo $style?>"></div>
						<div id="centro<?php echo $style?>" style="font-family: frutiger">

							<?php 
							// Recupero il titolo del forum dal DB
							$query_forum = $conn->prepare("SELECT * FROM forum_lite_main WHERE id = '" . $_GET['f'] . "'");
							$query_forum->execute();
							while($result_query_forum = $query_forum->fetch(PDO::FETCH_ASSOC)){
								$result_forum = $result_query_forum;
							}
							// Stampo il percorso
							if ($style == "_blue"){
								echo "<br><a href=\"forum_index?lingua=$lingua\"><button class='btn btn-outline-primary'>Main</button></a> » ";
								echo "<a href=\"forum?f=$_GET[f]&lingua=$lingua\">";
								echo "<button class='btn btn-outline-primary'>$result_forum[titolo]</button></a> »".$newtopic;
							} else {
								echo "<br><a href=\"forum_index?lingua=$lingua\"><button class='btn btn-outline-danger'>Main</button></a> » ";
								echo "<a href=\"forum?f=$_GET[f]&lingua=$lingua\">";
								echo "<button class='btn btn-outline-danger'>$result_forum[titolo]</button></a> »".$newtopic;	
							}
							// Se il valore di cmd è false stampo il form a video
							if (!isset($_POST['cmd'])) $cmd=FALSE;
							else $cmd=$_POST['cmd'];
							
							$autore='(Guest) ';
							
							if ($cmd == FALSE && isset($_SERVER['REQUEST_URI'])) {
								echo "<form action=\"$_SERVER[REQUEST_URI]\" method=\"post\">\n";
								echo $titolo;
								echo "<input type=\"text\" name=\"titolo\"><br><br>\n\n";
								echo "<strong>Nickname</strong>:<br>\n";
								if (!isset($_SESSION['Username'])) echo "<input type=\"text\" name=\"autore\"><br><br>\n\n";
								else echo "<input type=\"text\" name=\"autore\" disabled><br><br>\n\n";
								echo $messaggio;
								$browser = $_SERVER['HTTP_USER_AGENT'];
								$firefox = '/Firefox/';
								if (preg_match($firefox, $browser))
								echo "<div style='margin-left:40px;margin-right:40px;'><textarea name=\"testo\" cols=\"87\" rows=\"5\" style='resize:none;'>";
								else echo "<div style='margin-left:40px;margin-right:40px;'><textarea name=\"testo\" cols=\"90\" rows=\"5\" style='resize:none;'>";
								echo "</textarea></div><br><br>\n\n";
								echo "<input type=\"hidden\" name=\"cmd\" value=\"add\">\n";
								if ($style == "_blue"){
									echo "<input type=\"submit\" class='btn btn-primary' value=\"".$sendtopic."\">\n";
								}
								else echo "<input type=\"submit\" class='btn btn-danger' value=\"".$sendtopic."\">\n";
								echo "</form>\n";
							}

							// Se cmd è diverso da false...
							else {
								if (!isset($_SESSION['Username']))
									$autore="(Guest) ".$_POST['autore'];
								else {
									//// VERIFICA ADMIN ///
									$admincheck = $conn->prepare("SELECT priviledge FROM accounts WHERE szUserID='".$_SESSION['Username']."'  AND priviledge='admin'");	
									$admincheck->execute();
									if($admincheck->rowCount()==1) {
										$autore='<b>[Admin]</b>&nbsp'.$_SESSION['Username'];
									}
									else $autore=$_SESSION['Username'];
								}

								// Verifico che tutti i campi necessari siano stati compilati
								if ($_POST['titolo'] == FALSE OR $autore == '(Ospite) ' OR $_POST['testo'] == FALSE) {
									echo "<p>Tutti i campi sono obbligatori.";
								}

								// Se il controllo è ok salvo tutto nel DB
								else {
									$_POST['testo'] = str_replace("\n", "<br>", $_POST['testo']);
									$_POST['testo'] = str_replace("'", "><_", $_POST['testo']);
									$_POST['titolo'] = str_replace("'", "><_", $_POST['titolo']);

									$query_topic = $conn->prepare("INSERT INTO forum_lite_topics
												VALUES ('" . $_GET['t'] . "',
														'" . $_GET['f'] . "',
														'" . date("d/m/Y") . "',
														'" . $autore . "',
														'" . $_POST['titolo'] . "')");
									$query_topic->execute();

									$query_post = $conn->prepare("INSERT INTO forum_lite_thread
												VALUES ('" . $_GET['f'] . "',
														'" . $_GET['t'] . "',
														'" . date("d/m/Y") . "',
														'" . $autore . "',
														'" . $_POST['titolo'] . "',
														'" . $_POST['testo'] . "')");


									$query_post->execute();
									echo $successtopic;
									echo "<br><div class='loading".$style."'></div><br><br><br><br>
									<meta http-equiv='refresh' content='3; URL=forum?f=".$_GET['f']."&lingua=$lingua'>";
								}
							}?>
							
						</div>
						<div id="sotto<?php echo $style?>"></div>			
					</div>
					<!--Distanza-->
					<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
					
				</div><!--Body centro-->			
				<div id="body_sotto<?php echo $style?>"></div>
			</div>
		</div>
	</body>
</html>