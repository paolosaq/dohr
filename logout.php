<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?php
session_start();
if(!isset($_SESSION["Username"])) {
	$condotta = 0;
}
else {
	$condotta = 1;
}

/// CONTROLLO SERVER OFFLINE ///
if (!@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)){
	session_destroy();
	$condotta = 0;
}

include("includes/time.php");
include("includes/config.php");
include("gestorelingua.php");

$check = 0; 
$count = 0;
$style="_blue";

$changebackgroundred = <<< EOD
<div>
<script>
document.body.style.backgroundImage = 'url("backdrariva.jpg")';
</script>
</div>
EOD;

if($_SESSION["Faction"] == "Providentia"){
    $style="_blue";
} 
else if($_SESSION["Faction"] == "Drariva"){
    $style="_red";
    echo $changebackgroundred;
}
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style/style.css">
		<link  id="favicon" href="dragon.png" rel="shortcut icon" type="image/x-icon" />
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
		<meta name="keywords" content="<?php echo $CMS_SERVER_KEYWORDS; ?>"/>
		<link rel="shortcut icon" type="img/png" href="dragon.png"/>
		<title><?php echo $CMS_SERVER_NAME;?></title>
	</head>
	<body>
		<div id="main">
			<div id="wrapper">		
				<div id="header">		
					<div id="LogoIMG<?php echo $style?>"></div>		
				</div><!--Header-->		
				<div id="body_sopra"></div>
				<div id="body_centro_confirm">
					<div id="Content">			
						<div id="sopra<?php echo $style?>"></div>
						<div id="centro<?php echo $style?>">
							
							<?php
								if($condotta == 1)
								{
									session_destroy();
									echo $disconnect;
									echo "<br><div class='loading".$style."'></div><br><br><br><br>
									<meta http-equiv='refresh' content='3; URL=index?lingua=".$lingua."'>";						
								}
								else
								{
									echo $alreadydisconnected;
									echo "<br><div class='loading".$style."'></div><br><br><br><br>
									<meta http-equiv='refresh' content='3; URL=index?lingua=".$lingua."'>";
								}
							?>
							
						</div>
						<div id="sotto<?php echo $style?>"></div>			
					</div>
					
					<!--Divisione-->
					<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
					
				</div><!--Body centro-->			
				<div id="body_sotto"></div>
			</div>
		</div>
	</body>
</html>