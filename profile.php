<?php 
//include("gestorelingua.php");

if (@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)) {
    
    ///// PROFILO E GILDE /////
    ob_start(); 
    if(!isset($_SESSION["Username"]) and !isset($_GET["page"])) {
        $condotta = 0;
    }
    else {
        $condotta = 1;
    }
    
    if($condotta == 1) {
        if($_SESSION["Faction"] == "Providentia") {
            $style="_blue";
        }
        else if ($_SESSION["Faction"] == "Drariva") {
            $style="_red";
        }
        
        ////// CHECK ITEMS //////
        include_once("includes/checkitems.php");
    }
    else {
        $style="_blue";
    }
    
    /// Check Gilde ///
    if (isset($_POST['ruolo'])) {
        $gild_name= $_POST['gilde'];
        $nome=$_SESSION["Username"];
        $ruolo=$_POST['ruolo'];
        
        /// Controllo pre-inserimento ///
        $stmt = $conn->prepare("SELECT membro FROM ".$gild_name." WHERE membro='".$nome."'");
        $stmt->execute();
        if(($stmt->rowCount()!=1)) {
            $q = $conn->prepare("INSERT INTO ".$gild_name." VALUES ('".$nome."','".$ruolo."');");
            $q->execute();
            $q2 =$conn->prepare("UPDATE accounts SET gilda='".$gild_name."' WHERE szUserID='".$nome."'");
            $q2->execute();
        }
    }
    
    if(isset($_POST['exit'])) {
        $nome=$_SESSION["Username"];
        $gild_name=$_SESSION['Gilda'];
        $q = $conn->prepare("DELETE FROM ".$gild_name." WHERE membro='".$nome."';");
        $q->execute();
        $q2 = $conn->prepare("UPDATE accounts SET gilda='' WHERE szUserID='".$nome."'");
        $q2->execute();
        unset($_POST['exit']);
    }

    $profile = ob_get_clean();
    
?>


<div>
    <head>
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
    </head>
    
    <?php
    /// UPLOAD FOTO PROFILO ///
    if(isset($_FILES['file'])){
        move_uploaded_file($_FILES['file']['tmp_name'],"pictures/".$_FILES['file']['name']);
        $q = $conn->prepare("UPDATE accounts SET image = '".$_FILES['file']['name']."' WHERE szUserID = '".$_SESSION['Username']."'");
        $q->execute();
    }
    
    ///////// READING GUILD //////////
    if($condotta == 1){
        $stmt = $conn->prepare("SELECT gilda FROM accounts WHERE szUserID='".$_SESSION["Username"]."'");
        $stmt->execute();
        while ($checkguild = $stmt->fetch(\PDO::FETCH_ASSOC)){
            $_SESSION["Gilda"] = $checkguild['gilda'];
        }
        
        if($_SESSION["Gilda"] != '') {
            $role_query = $conn->prepare("SELECT ruolo FROM ".$_SESSION["Gilda"]." WHERE membro='".$_SESSION["Username"]."'");
            $role_query->execute();
            while ($rolecheck = $role_query->fetch(\PDO::FETCH_ASSOC)){
                $role = $rolecheck['ruolo'];
            }
        }
    }
    ?>
    
    <body>
        <!-- GILDE -->
        <div id="guilds" class="w3-modal">
            <div class="w3-modal-content">
                <div class="w3-container">
                    <span onclick="document.getElementById('guilds').style.display='none' ;
                    document.getElementById('guildselector').style.display='none';
                    document.getElementById('guildroles').style.display='none';
                    document.getElementById('zonaDinamica').innerHTML='';
                    document.getElementById('exitguildout').style.display='inline-block'" class="w3-button w3-display-topright">&times;
                    </span>
                    
                    <?php echo $guildtitle;?>
                    <br><br>
                    <hr style='width:95%;'>
                    <br><br>
                    <!--ICONE GILDE-->
                    <div class="warlock" onclick="warlock();document.getElementById('exitguildout').style.display='none';";></div>
                    <div class="paladin" onclick="paladin();document.getElementById('exitguildout').style.display='none';"></div>
                    <div class="wise" onclick="wise();document.getElementById('exitguildout').style.display='none';"></div>
                    <div class="guardian" onclick="guardian();document.getElementById('exitguildout').style.display='none';"></div>
                    <div class="seeker" onclick="seeker();document.getElementById('exitguildout').style.display='none';"></div>
                    <br><br>
                    
                    <div id="guildselector">
                        <div id ="title">Vuoto</div>
                        <br><br>
                        <div id="zonaDinamica" class="dynamic_member"></div>
                        <br><br>
                        <div id="bottonemembri" class="members" onclick="caricaDocumento(this.innerHTML)" data-toggle="tooltip" data-placement="top" title="<?php echo $members; ?>">
                        </div>
                        <br>
                        <script>
                        
                            function caricaDocumento(e1) {
                                var xhttp = new XMLHttpRequest();
                                xhttp.onreadystatechange = function() {
                                    if (this.readyState == 4 && this.status == 200) {
                                        document.getElementById("zonaDinamica").innerHTML = this.responseText;
                                    }
                                };
                                xhttp.open("GET", e1, true);
                                xhttp.send();
                            }
                        
                        </script>
                        
                        <form action='' method='post'>
                            <?php
                            if($_SESSION["Gilda"] == '') {?>
                                <div class="enter" onclick="displayroles();" data-toggle="tooltip"  title="<?php echo $joinGuild; ?>"></div>
                                
                            <?php } ?>
                            
                            <?php
                            if($_SESSION["Gilda"] != '') {?>
                                <input type="submit" class="exit" id="exitguild" name="exit" value="" data-toggle="tooltip"  title="<?php echo $exitGuild; ?>">
                                
                            <?php } ?>
                            
                            <br>
                            <input type="hidden" id="gilda" name="gilde" value="">
                            <div id="guildroles">
                                <br><br>
                                <b><?php echo $selectRole; ?></b>
                                <br><br>
                                <input type="submit" class="attacker" name="ruolo" value="Attacker" data-toggle="tooltip"  title="<?php echo $attackerRole; ?>">
                                <input type="submit" class="defender" name="ruolo" value="Defender" data-toggle="tooltip"  title="<?php echo $defenderRole; ?>">
                                <input type="submit" class="sorcerer" name="ruolo" value="Sorcerer" data-toggle="tooltip"  title="<?php echo $magicRole; ?>">
                                <input type="submit" class="healer" name="ruolo" value="Healer" data-toggle="tooltip"  title="<?php echo $healerRole; ?>">
                            </div>
                        </form>
                        <br><br><br><br><br>
                    </div>
                    
                    <form action="" method="post">
                        <?php
                        if($_SESSION["Gilda"] != '') {?>
                            <input type="submit" class="exit" id="exitguildout" name="exit" value="" data-toggle="tooltip"  title="<?php echo $exitGuild; ?>" >
                            
                        <?php } ?>
                    </form>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
            </div> <!-- MODAL CONTENT -->
        </div> <!-- GUILDS -->
        
        <!-- PROFILO NON NOME UTENTE -->
        <div id="profile" class="w3-modal">
            <div class="w3-modal-content">
                <div class="w3-container">
                    <span onclick="document.getElementById('profile').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                    <div class="profile_username<?php echo $style?>">
                        <?php echo $_SESSION["Username"] ?>
                    </div><br><br>
                    <hr style='width:35%;'><br>
                
                    <div id="profile_picture">
                        <form class="select-image" action="" method="post" enctype="multipart/form-data">
                            <div class="image-upload">
                                <label for="file-input">
                                    <div class="cameraicon"></div>
                                </label>
                                
                                <?php
                                $q = $conn->prepare("SELECT * FROM accounts");
                                $q->execute();
                                
                                while($row2 = $q->fetch(PDO::FETCH_ASSOC)){
                                    $row = $row2;
                                    if($row['szUserID'] == $_SESSION["Username"]){
                                        if($row['image'] == ""){
                                            echo"<img class='circular--square' src='/pictures/default.jpg' alt='Default Profile Pic'>";
                                        }
                                        else {
                                            echo"<img class='circular--square' src='/pictures/".$row['image']."' alt='Profile Pic'>";
                                        }
                                    }
                                }
                                
                                ?>
                                
                                <input id="file-input" type="file" name="file" onchange='this.form.submit();'/>
                            </div>
                        </form>
                    </div>
                    <br><br><br>
                
                    <div class="profile_info">
                        <?php echo $faction; ?> : <b> <?php echo $_SESSION["Faction"]?></b><br>
                        <?php echo $guild; ?> : 
                        
                        <b> <?php 
                        
                            if ($_SESSION["Gilda"] != "") {

                                /// CHECK RUOLO ///
                                if($role == "Attacker") $role = $attackerRole;
                                else if ($role == "Defender") $role = $defenderRole;
                                else if ($role == "Sorcerer") $role = $magicRole;
                                else if ($role == "Healer") $role = $healerRole;

                                /// CHECK GILDA ///
                                if($_SESSION["Gilda"] == "wise"){ $namefactionchat = $wise; echo $namefactionchat.'   <br></b>'.$guildRole.': <b>'.$role.'';}
                                else if($_SESSION["Gilda"] == "warlock"){ $namefactionchat = $warlock; echo $namefactionchat.'   <br></b>'.$guildRole.': <b>'.$role.'';}
                                else if($_SESSION["Gilda"] == "guardian"){ $namefactionchat = $guardian; echo $namefactionchat.'   <br></b>'.$guildRole.': <b>'.$role.'';}
                                else if($_SESSION["Gilda"] == "paladin") { $namefactionchat = $paladin; echo $namefactionchat."  <br></b>'.$guildRole.': <b>".$role."";}
                                else if ($_SESSION["Gilda"] == "seeker"){ $namefactionchat = $seeker; echo $namefactionchat.'  <br></b>'.$guildRole.': <b>'.$role.'';}
                            }
                            else  echo $none;
                        ?></b>
                    </div>
                    <br><br><br><br>
                    <hr style='width:85%;'>
                    <div class="profile_info">
                        <?php echo $inventory.':';?>
                    </div>
                
                    <div id="inventory">
                        <div id="health"></div>
                        <div id="textleft">x<?php echo $health;?></div>
                        <div id="mana"></div>
                        <div id="textright">x<?php echo $mana;?></div>
                        <br>
                        <div id="stamina"></div>
                        <div id="textleft">x<?php echo $stamina;?></div>
                        <div id="dice"></div>
                        <div id="textright">x<?php echo $dices;?></div>
                        <br>
                        <div id="redsmith"></div>
                        <div id="textleft">x<?php echo $red;?></div>
                        <div id="realmswarden"></div>
                        <div id="textright">x<?php echo $realm;?></div>
                        <br>
                        <div id="metamedallion"></div>
                        <div id="textleft">x<?php echo $meta;?></div>
                        <div id="hushedring"></div>
                        <div id="textright">x<?php echo $hush;?></div>
                        <br>
                        <div id="spellwriting"></div>
                        <div id="textleft">x<?php echo $spell;?></div>
                        <div id="eternalslayer"></div>
                        <div id="textright">x<?php echo $eternal;?></div>
                        <br>
                        <div id="fallingstars"></div>
                        <div id="textleft">x<?php echo $stars;?></div>
                        <div id="oath"></div>
                        <div id="textright">x<?php echo $oath;?></div>
                        <br>
                        <div id="dreamelixir"></div>
                        <div id="textleft">x<?php echo $dream;?></div>
                        <div id="noname"></div>
                        <div id="textright">x<?php echo $noname;?></div>
                        <br>
                    </div>
                    <br><br><br><br>
                </div>
            </div>
        </div>
    </body>
</div>


<?php 
//$profile = ob_get_clean(); 
}
?>
<!-- //// SCRIPT PER GILDE //// -->
<script>
    function displayroles(){
   document.getElementById('guildroles').style.display='block';
}

function warlock(){ 
   document.getElementById("bottonemembri").innerHTML = "includes/listamembri?g=warlock";
   document.getElementById('zonaDinamica').innerHTML='';
   document.getElementById('guildselector').style.display='inline-block';
   document.getElementById("title").innerHTML = "<?php echo $warlock; ?>";
   document.getElementById("gilda").setAttribute('value','warlock');
   document.getElementById("exitguild").setAttribute('value','warlock');
} 

function guardian(){ 
   document.getElementById("bottonemembri").innerHTML = "includes/listamembri?g=guardian";
   document.getElementById('zonaDinamica').innerHTML='';
   document.getElementById('guildselector').style.display='inline-block';
   document.getElementById("title").innerHTML = "<?php echo $guardian; ?>";
   document.getElementById("gilda").setAttribute('value','guardian');
   document.getElementById("exitguild").setAttribute('value','guardian');
} 

function seeker(){ 
   document.getElementById("bottonemembri").innerHTML = "includes/listamembri?g=seeker";
   document.getElementById('zonaDinamica').innerHTML='';
   document.getElementById('guildselector').style.display='inline-block';
   document.getElementById("title").innerHTML = "<?php echo $seeker; ?>";
   document.getElementById("gilda").setAttribute('value','seeker');
   document.getElementById("exitguild").setAttribute('value','seeker');
} 


function wise(){
   document.getElementById("bottonemembri").innerHTML = "includes/listamembri?g=wise";
   document.getElementById('zonaDinamica').innerHTML='';
   document.getElementById('guildselector').style.display='inline-block';
   document.getElementById("title").innerHTML = "<?php echo $wise; ?>";
   document.getElementById("gilda").setAttribute('value','wise');
   document.getElementById("exitguild").setAttribute('value','wise');
} 


function paladin(){    
   document.getElementById("bottonemembri").innerHTML = "includes/listamembri?g=paladin";
   document.getElementById('zonaDinamica').innerHTML='';
   document.getElementById('guildselector').style.display='inline-block';
   document.getElementById("title").innerHTML = "<?php echo $paladin; ?>";
   document.getElementById("gilda").setAttribute('value','paladin');
   document.getElementById("exitguild").setAttribute('value','paladin');
}
</script>