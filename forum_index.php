<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?php
session_start();
if(!isset($_SESSION["Username"])) {
	$condotta = 0;
}
else {
	$condotta = 1;
}

/// CONTROLLO SERVER OFFLINE ///
if (!@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)){
	session_destroy();
	$condotta = 0;
}

include("includes/time.php");
include("includes/config.php");
include("gestorelingua.php");

include("profile.php");

$count = 0;
$style="_blue";

$changebackgroundred = <<< EOD
<div>
<script>
document.body.style.backgroundImage = 'url("backdrariva.jpg")';
</script>
</div>
EOD;
?>

<html>
	<head>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>
	    <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v6.0&appId=286450078046373"></script>
		<link rel="stylesheet" type="text/css" href="style/style.css">
		<meta name="viewport" content="width-device-width, initial scale=1"/>
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
		<meta name="keywords" content="<?php echo $CMS_SERVER_KEYWORDS; ?>"/>
		<title><?php echo $CMS_SERVER_NAME;?></title>
	</head>

	<body onload='document.getElementById("body_centro").style.opacity="1" ; startTime()'>

		<?php 
		echo $profile;
		if($condotta == 1 && $_SESSION["Faction"] == "Providentia"){
			$style="_blue";
		} 
		else if($condotta == 1 && $_SESSION["Faction"] == "Drariva"){
			$style="_red";
			echo $changebackgroundred;	
		} 
		?>

		<div id="main">
			<div id="wrapper">
				
				<div id="login">
					<div id="LoginIMGBG<?php echo $style?>">
						<?php 
						if($condotta == 0) { ?>
							<form action="login?lingua=<?php echo $lingua; ?>" method="POST">
								<!--Login Box-->
								<div id="loginbox<?php echo $style?>">
									<div id="login-box-field"><input name="UsernameS" class="form-login" title="Username" placeholder='Username' size="30" maxlength="17" required/></div>
								</div>
								<!--Password Box-->					
								<div id="passwordbox<?php echo $style?>">
									<div id="login-box-field"><input name="PasswordS" type="password" class="form-login" title="Password" placeholder="******" size="30" maxlength="17" required/></div>
								</div>
								<!--Send-->
								<?php 
								if (@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)) { ?>
									<input type="submit" name="test" value="" class="loginbutton<?php echo $style?>">
								<?php } 
								else { ?>
									<div class="nologin"></div>
								<?php } ?>
							</form>	
						<?php 
						}

						if($condotta == 1) {

							///////// READING GOLD //////////
							$stmt = $conn->prepare("SELECT gold FROM accounts WHERE szUserID='".$_SESSION["Username"]."'");
							$stmt->execute();
							while ($checkgold = $stmt->fetch(\PDO::FETCH_ASSOC)) {
								$_SESSION["Gold"] = $checkgold['gold'];
							}
							

							///////// READING UNITS //////////
							$stmt = $conn->prepare("SELECT units FROM accounts WHERE szUserID='".$_SESSION["Username"]."'");
							$stmt->execute();
							while ($checkunit = $stmt->fetch(\PDO::FETCH_ASSOC)) {
								$_SESSION["Units"] = $checkunit['units'];
							}

							
							///////// CHECK IF YOU OVERCOME MONEY LIMIT //////////
							if( $_SESSION["Units"] > 99999) {
								$stmt= $conn->prepare("UPDATE accounts SET units = 99999 WHERE szUserID='".$_SESSION["Username"]."'");
								$stmt->execute();
								while($checkunit = $stmt->fetch(\PDO::FETCH_ASSOC)) {
									$_SESSION["Units"] = $checkunit['units'];
								}
							}
							if($_SESSION["Gold"] > 999999) {
								$stmt= $conn->prepare("UPDATE accounts SET gold = 999999 WHERE szUserID='".$_SESSION["Username"]."'");
								$stmt->execute();
								while ($checkgold = $stmt->fetch(\PDO::FETCH_ASSOC)) {
									$_SESSION["Gold"] = $checkgold['gold'];
								}
							}

							///////// CHECK FACTION //////////

							if($_SESSION["Faction"] == "Providentia")
								echo'<div class="userprovidentia"></div>';
							else { 
								echo '<div class="userdrariva"></div>'; 
							}

						?>

							<!-- SIMBOLO GILDA -->
							<div class="guild" onclick="document.getElementById('guilds').style.display='block'"></div>
							
							<!-- NOME UTENTE -->
							<div id="logindone<?php echo $style?>" onclick="document.getElementById('profile').style.display='block'">
								<?php
								echo $_SESSION["Username"]; 
								?>			
							</div>


							<!-- VALUTA -->
							<?php
							if($_SESSION["Gold"] < 20000)
								echo'<div class="lowgold"></div>';
							else if ($_SESSION["Gold"] < 60000){ 
								echo '<div class="mediumgold"></div>'; 
							}
							else{
								echo '<div class="highgold"></div>'; 
							}
							?>

							<?php echo 
							'<div class="goldtext">'.$_SESSION["Gold"].'</div>'; 
							?>	
							<div class="units"></div>	
							<?php echo 
							'<div class="unitstext">'.$_SESSION["Units"].'</div>'; 
							?>

							<!-- LOGOUT -->
							<a href="logout?lingua=<?php echo $lingua; ?>" id="logout"><div class="logoutbutton<?php echo $style?>"></div></a>

						<?php 
						}
						?>

						<?php
						///////// LINGUE //////////

						foreach ($lingue as $k=>$v) {
							if ($k != $lingua) {
								if($condotta==0) { ?>
									<a href="?lingua=<?php echo $k; ?>">
									<img src="bandiere/<?php echo $flags[$k]; ?>.gif" alt="<?php echo $v; ?>" title="<?php echo $v;?>" 
									style="float: right; margin-top: 15px; margin-right:20px" />
									</a>
								<?php 
								}
								?>

								<?php
								if($condotta==1) { ?>
									<a href="?lingua=<?php echo $k; ?>">
									<img src="bandiere/<?php echo $flags[$k]; ?>.gif" alt="<?php echo $v; ?>" title="<?php echo $v;?>" 
									style="float: right; margin-top: -10px; margin-right:30px" />
									</a>
								<?php
								}
							}
						}
						?>

						<!-- GOOGLE TRADUTTORE
						<div id="google_translate_element"></div>
						
						<script type="text/javascript">
							function googleTranslateElementInit() {
								new google.translate.TranslateElement({pageLanguage: 'it'},'google_translate_element');
							}
							
						</script>
						
						<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">
						</script>
						
						GOOGLE TRADUTTORE -->

					</div> <!--LoginIMGBG-->
				</div><!--Login-->

				<div id="header">		
					<div id="LogoIMG<?php echo $style?>"></div>		
				</div><!--Header-->	

				<div id="body_sopra">	
					<div id="MenueIMG<?php echo $style?>">
						<div id="navi"style="font-family: enchant; font-size: 21px;">			
							<a href="index?lingua=<?php echo $lingua; ?>" onclick='document.getElementById("body_centro").style.opacity="0"'><li><?php echo $HOME ?></li></a>
							<a href="forum_index?lingua=<?php echo $lingua; ?>" onclick='document.getElementById("body_centro").style.opacity="0"'><li><?php echo $FORUM ?></li></a>
							<a href="download?lingua=<?php echo $lingua; ?>" onclick='document.getElementById("body_centro").style.opacity="0"'><li><?php echo $DOWNLOAD ?></li></a>
							<a href="shop?lingua=<?php echo $lingua; ?>" onclick='document.getElementById("body_centro").style.opacity="0"'><li><?php echo $SHOP ?></li></a>
							<a href="factions?lingua=<?php echo $lingua; ?>" onclick='document.getElementById("body_centro").style.opacity="0"'><li><?php echo $FAZIONI ?></li></a>	
							<a href="donations?lingua=<?php echo $lingua; ?>" onclick='document.getElementById("body_centro").style.opacity="0"'><li><?php echo $DONATIONS ?></li></a>	
							<a href="register?lingua=<?php echo $lingua; ?>" onclick='document.getElementById("body_centro").style.opacity="0"'><li><?php echo $REGISTRAZIONE ?></li></a>
						</div>					
					</div>			
				</div><!--Body sopra-->	

				<div id="body_centro">
					<div id="SlideIMG<?php echo $style?>"></div>
					<div id="LinksMenu">			
						<div id="sopra<?php echo $style?>"></div>

						<div id="centro<?php echo $style?>">				
							<div id="ServerInfos<?php echo $style?>" style="font-family: enchant; font-size: 20px;">
								<div id="text">
									<br>
									<?php
									if (!@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)){ echo $servertimeoffline;}
									else{echo $servertime;
										echo $tempo;
										echo "</b>";}
									?>
									<?php echo $serverstatus;
										if (!@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)){
											echo "<b>Offline</b>"; 
											} else  echo "<b>Online</b>";  ?>								
									<br>
									<?php echo $registered;
										if (!@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)) echo '<b>Server Offline</b>';
										else {
											while ($rows = $resultnumber->fetch(\PDO::FETCH_ASSOC)){
												$count++;
											} 
											echo "<b>".$count."</b>";
											}?>
									<br>													
									EXP Rate<font size='3' face='frutiger'>: <?php echo "<b>".$CMS_EXP_RATE."</b>"?>	</font><br>
									Drop Rate<font size='3' face='frutiger'>: <?php echo "<b>".$CMS_DROP_RATE."</b>"?>	</font><br>
									UP Rate<font size='3' face='frutiger'>:<?php echo "<b>".$CMS_UP_RATE."</b>"?>	</font><br>
									<br>
								</div>
							</div><!--ServerInfos-->
							<!--SOCIAL-->	
							<div class="fb-page" data-href="https://www.facebook.com/profile.php?id=100087497641513" data-tabs="" data-width="250px" data-height="97px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
								<blockquote cite="https://www.facebook.com/profile.php?id=100087497641513" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/profile.php?id=100087497641513">Dohr</a>
								</blockquote>
							</div>
							<br>
							<div class="instaback"><a href="https://www.instagram.com/dohrcampaign/"target="_blank"><div id="instagram"></div></a></div>
							<iframe width="250" height="137" src="https://www.youtube.com/embed/bXbjSAv0OsI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div><!--centro-->	

						<div id="sotto<?php echo $style?>"></div>				
					</div> <!--linksmenu-->	

					<div id="Content">			
						<div id="sopra<?php echo $style?>"></div>
						<div id="centro<?php echo $style?>">
						
							<br><br><br>
							<?php

							if (@$fp = fsockopen("localhost", 3306, $errno, $errstr, 1)){

								// Recupero i forum esistenti
								$check= $conn->prepare("SELECT * FROM forum_lite_main GROUP BY titolo ASC");
								$check->execute();

								// Stampo la lista di tutti i Forum
								while($result =$check->fetch(PDO::FETCH_ASSOC)){
									$resultforum = $result["id"];
								}

								if ($check){
									echo "<a href=\"forum?f=$resultforum&lingua=$lingua\" onclick='document.getElementById('body_centro').style.opacity='0''>";
									if($style=="_blue"){
										echo '<div class="forum_blue" data-toggle="tooltip" title="'.$enterforum.'"></div></a>'; 
									}
									else{
										echo '<div class="forum_red" data-toggle="tooltip" title="'.$enterforum.'"></div></a>'; 
									}
									echo $forumintro;
								}
							}
							else echo $forumclosed;
							?>
							<br><br><br>
						
						</div>
						<div id="sotto<?php echo $style?>"></div>			
					</div>
					
					<!--Distanza-->
					<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>	
				</div><!--Body centro-->

				<div id="body_sotto<?php echo $style?>"></div><!--Body sotto-->
			</div><!--wrapper-->
		</div><!--main-->


		<!--CHAT-->
		<?php 
		if (isset($_SESSION['Username']) && $_SESSION['Gilda'] != "" && $condotta==1) {
			$chat=$_SESSION["Gilda"]; ?>
			<button class="open-button<?php echo $style?>" onclick="openForm(); scrollDownMessages()"><?php echo "<b>Chat ".$namefactionchat."</b>"?>
				<span id="dotpopup" class="dot"></span>
				<audio id='notify' src='chat/notification.mp3'></audio>
			</button>

			<div class="chat-popup" id="myForm" style="font-family: frutiger">
				<form action="/action_page.php" class="form-container">
					<h1><?php echo $namefactionchat?></h1>

					<label for="msg"><b><?php echo $chatmessages; ?></label>
					<div type="box" class="messaggi" id="messaggi"></div></b>
					<textarea placeholder="<?php echo $writemessage; ?>" name="msg" id="msg" onkeyup="success()" required></textarea>

					<button type="button" id="send" class="btn" value="?chat=<?php echo $chat?>" onclick="caricaDocumentoChat(this.value)" disabled>  <?php echo $sendmessagechat; ?> </button>
					<button type="button" class="btn cancel" onclick="closeForm()"><?php echo $closechat; ?></button>
				</form>
			</div>

			<script>

			var len0 = 0;
			var notify = document.getElementById("notify");
			var input = document.getElementById("msg");

			setInterval(refreshMessages, 1000);

			//Invio messaggio con Enter
			input.addEventListener("keyup", function(event) {
				if (event.keyCode === 13) {
					event.preventDefault();
					document.getElementById("send").click();
				}
			});

			function openForm() {
				document.getElementById('dotpopup').style.display = 'none';
				document.getElementById("myForm").style.display = "block";
			}

			function closeForm() {
				document.getElementById('dotpopup').style.display = 'none';
				document.getElementById("myForm").style.display = "none";
			}

			function success() {
				if(document.getElementById("msg").value==="") {
					document.getElementById('send').disabled = true;
				} else {
					document.getElementById('send').disabled = false;
				}
			}

			function caricaDocumentoChat(chatName) {
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
					document.getElementById("messaggi").innerHTML = this.responseText;
					}
				};
				msg="&msg="+document.getElementById("msg").value;
				xhttp.open("GET", "chat/chatbox"+chatName+msg, true);
				xhttp.send();
				document.getElementById("msg").value="";
			}

			function refreshMessages() {
				var messages = document.getElementById("messaggi").innerHTML;
				var myArray = messages.split(' ');
				var len = 0;
				for(var i in myArray) { 
					len += myArray[ i ].length 
				}
				if (len > len0){
					if (len0 != 0) {	
					notify.play();
					document.getElementById("dotpopup").style.display = 'inline-block';
					}
					len0=len;
					$('#messaggi').load("chat/update?chat=<?php echo $chat ?>");
					$("#messaggi").animate({ scrollTop: $(document).height() }, "fast");
				}
				else {
					$('#messaggi').load("chat/update?chat=<?php echo $chat ?>")
				};
			}

			function scrollDownMessages(){
				$("#messaggi").animate({ scrollTop: $(document).height() }, "fast");
			}

			</script>

		<?php } ?>
		<!--FINE CHAT-->

	</body>
</html>