//// HEALTH POTIONS ////

function buttonplushealth() {
  if(document.getElementById('healthquantity').innerHTML != 99){
    document.getElementById('healthquantity').innerHTML++;
    document.getElementById('healthquantitytrue').value++;
  }
}

function buttonminushealth() {
  if(document.getElementById('healthquantity').innerHTML != 0){
    document.getElementById('healthquantity').innerHTML--;
    document.getElementById('healthquantitytrue').value--;
  }
}

//// MANA POTIONS ////

function buttonplusmana() {
  if(document.getElementById('manaquantity').innerHTML != 99){
    document.getElementById('manaquantity').innerHTML++;
    document.getElementById('manaquantitytrue').value++;
  }
}

function buttonminusmana() {
  if(document.getElementById('manaquantity').innerHTML != 0){
    document.getElementById('manaquantity').innerHTML--;
    document.getElementById('manaquantitytrue').value--;
  }
}

//// STAMINA POTIONS ////

function buttonplusstamina() {
  if(document.getElementById('staminaquantity').innerHTML != 99){
    document.getElementById('staminaquantity').innerHTML++;
    document.getElementById('staminaquantitytrue').value++;
  }
}

function buttonminusstamina() {
  if(document.getElementById('staminaquantity').innerHTML != 0){
    document.getElementById('staminaquantity').innerHTML--;
    document.getElementById('staminaquantitytrue').value--;
  }
}

//// IMAGE DICES ////

function buttonplusdices() {
  if(document.getElementById('dicequantity').innerHTML != 99){
    document.getElementById('dicequantity').innerHTML++;
    document.getElementById('dicequantitytrue').value++;
  }
}

function buttonminusdices() {
  if(document.getElementById('dicequantity').innerHTML != 0){
    document.getElementById('dicequantity').innerHTML--;
    document.getElementById('dicequantitytrue').value--;
  }
}

//// METAMEDALLION ////

function buttonplusrosso() {
  if(document.getElementById('rossoquantity').innerHTML != 99){
    document.getElementById('rossoquantity').innerHTML++;
    document.getElementById('rossoquantitytrue').value++;
  }
}

function buttonminusrosso() {
  if(document.getElementById('rossoquantity').innerHTML != 0){
    document.getElementById('rossoquantity').innerHTML--;
    document.getElementById('rossoquantitytrue').value--;
  }
}

//// REALMSWARDEN ////

function buttonplusrealm() {
  if(document.getElementById('realmquantity').innerHTML != 99){
    document.getElementById('realmquantity').innerHTML++;
    document.getElementById('realmquantitytrue').value++;
  }
}

function buttonminusrealm() {
  if(document.getElementById('realmquantity').innerHTML != 0){
    document.getElementById('realmquantity').innerHTML--;
    document.getElementById('realmquantitytrue').value--;
  }
}

//// METAMEDALLION ////

function buttonplusmeta() {
  if(document.getElementById('metaquantity').innerHTML != 99){
    document.getElementById('metaquantity').innerHTML++;
    document.getElementById('metaquantitytrue').value++;
  }
}

function buttonminusmeta() {
  if(document.getElementById('metaquantity').innerHTML != 0){
    document.getElementById('metaquantity').innerHTML--;
    document.getElementById('metaquantitytrue').value--;
  }
}

//// HUSHEDRING ////

function buttonplusring() {
  if(document.getElementById('ringquantity').innerHTML != 99){
    document.getElementById('ringquantity').innerHTML++;
    document.getElementById('ringquantitytrue').value++;
  }
}

function buttonminusring() {
  if(document.getElementById('ringquantity').innerHTML != 0){
    document.getElementById('ringquantity').innerHTML--;
    document.getElementById('ringquantitytrue').value--;
  }
}

//// SPELLWRITING ////

function buttonpluswriting() {
  if(document.getElementById('spellquantity').innerHTML != 99){
    document.getElementById('spellquantity').innerHTML++;
    document.getElementById('spellquantitytrue').value++;
  }
}

function buttonminuswriting() {
  if(document.getElementById('spellquantity').innerHTML != 0){
    document.getElementById('spellquantity').innerHTML--;
    document.getElementById('dicequantitytrue').value--;
  }
}

//// ETERNAL SLAYER ////

function buttonpluseternal() {
  if(document.getElementById('slayerquantity').innerHTML != 99){
    document.getElementById('slayerquantity').innerHTML++;
    document.getElementById('slayerquantitytrue').value++;
  }
}

function buttonminuseternal() {
  if(document.getElementById('slayerquantity').innerHTML != 0){
    document.getElementById('slayerquantity').innerHTML--;
    document.getElementById('slayerquantitytrue').value--;
  }
}

//// FALLING STARS ////

function buttonplusfalling() {
  if(document.getElementById('starsquantity').innerHTML != 99){
    document.getElementById('starsquantity').innerHTML++;
    document.getElementById('starsquantitytrue').value++;
  }
}

function buttonminusfalling() {
  if(document.getElementById('starsquantity').innerHTML != 0){
    document.getElementById('starsquantity').innerHTML--;
    document.getElementById('starsquantitytrue').value--;
  }
}

//// DREAM ELIXIR ////

function buttonpluselixir() {
  if(document.getElementById('elixirquantity').innerHTML != 99){
    document.getElementById('elixirquantity').innerHTML++;
    document.getElementById('elixirquantitytrue').value++;
  }
}

function buttonminuselixir() {
  if(document.getElementById('elixirquantity').innerHTML != 0){
    document.getElementById('elixirquantity').innerHTML--;
    document.getElementById('elixirquantitytrue').value--;
  }
}

//// OATH ////

function buttonplusoath() {
  if(document.getElementById('oathquantity').innerHTML != 99){
    document.getElementById('oathquantity').innerHTML++;
    document.getElementById('oathquantitytrue').value++;
  }
}

function buttonminusoath() {
  if(document.getElementById('oathquantity').innerHTML != 0){
    document.getElementById('oathquantity').innerHTML--;
    document.getElementById('oathquantitytrue').value--;
  }
}

//// NO NAME ////

function buttonplusnoname() {
  if(document.getElementById('nonamequantity').innerHTML != 99){
    document.getElementById('nonamequantity').innerHTML++;
    document.getElementById('nonamequantitytrue').value++;
  }
}

function buttonminusnoname() {
  if(document.getElementById('nonamequantity').innerHTML != 0){
    document.getElementById('nonamequantity').innerHTML--;
    document.getElementById('nonamequantitytrue').value--;
  }
}