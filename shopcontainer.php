<?php

$shopboxes= <<<EOD

<font size='5' face='frutiger'>
  <script src="includes/quantity.js"></script>
  
  <div id="id01" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id01').style.display='none' ; 
        document.getElementById('healthquantity').innerHTML=0; document.getElementById('healthquantitytrue').value=0"
        class="w3-button w3-display-topright">&times;</span>
        <div id="health"></div><br>     
        <font size='8' face='enchant'>
        $potionName
        </font><br>
        $potionDescript
        <br><br>
        <input type=button class="plusbutton" onclick="buttonplushealth()"/>
        <input type=button class="minusbutton" onclick="buttonminushealth()" />
        
        <br><br>

        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="healthquantity" class="itemquantity">0</span><br>
          <input id="healthquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="healthpotions">
          <input type="hidden" name="gold" value="500">
          <input type="hidden" name="units" value="1">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id02" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id02').style.display='none' ; document.getElementById('manaquantity').innerHTML=0
        document.getElementById('manaquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="mana"></div><br> 
        <font size='8' face='enchant'>
        $manaName
        </font><br>
        $manaDescript
        <br><br>
        <input type=button class="plusbutton" onclick="buttonplusmana()" />
        <input type=button class="minusbutton" onclick="buttonminusmana()" />
        
        <br><br>
        
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="manaquantity" class="itemquantity">0</span><br>
          <input id="manaquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="manapotions">
          <input type="hidden" name="gold" value="500">
          <input type="hidden" name="units" value="1">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id03" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id03').style.display='none' ; document.getElementById('staminaquantity').innerHTML=0
        document.getElementById('staminaquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="stamina"></div><br> 
        <font size='8' face='enchant'>
        $vigorName
        </font><br>
        $vigorDescript
        <br><br>
        <input type=button class="plusbutton" onclick="buttonplusstamina()" />
        <input type=button class="minusbutton" onclick="buttonminusstamina()" />
        
        <br><br>
        
        <form action="itembuy?lingua=$lingua" method="POST">
            <span id="staminaquantity" class="itemquantity">0</span><br>
            <input id="staminaquantitytrue" type="hidden" name="quantity" value="0">
            <input type="hidden" name="item" value="staminapotions">
            <input type="hidden" name="gold" value="1500">
            <input type="hidden" name="units" value="2">
            <input type="submit" name="action" class="buy_gold" value="gold">
            <input type="submit" name="action" class="buy_units" value="units" >
        </form>
            
            <br><br><br>
      </div>
    </div>
  </div>

  <div id="id04" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id04').style.display='none' ; document.getElementById('dicequantity').innerHTML=0
        document.getElementById('dicequantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="dice"></div><br>
        <font size='8' face='enchant'>
        $diceName
        </font><br>
        $diceDescript
        <br><br>
        <input type=button class="plusbutton" onclick="buttonplusdices()" />
        <input type=button class="minusbutton" onclick="buttonminusdices()" />
        
        <br><br>
        
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="dicequantity" class="itemquantity">0</span><br>
          <input id="dicequantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="imagedices">
          <input type="hidden" name="gold" value="8000">
          <input type="hidden" name="units" value="8">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id05" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id05').style.display='none'  ; document.getElementById('rossoquantity').innerHTML=0
        document.getElementById('rossoquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="redsmith"></div><br> 
        <font size='8' face='enchant'>
        $hammerName
        </font><br>
        $hammerDescript
        <br><br>
        <input type=button class="plusbutton" onclick="buttonplusrosso()" />
        <input type=button class="minusbutton" onclick="buttonminusrosso()" />
        
        <br><br>
                
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="rossoquantity" class="itemquantity">0</span><br>
          <input id="rossoquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="redsmith">
          <input type="hidden" name="gold" value="10000">
          <input type="hidden" name="units" value="10">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id06" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id06').style.display='none' ; document.getElementById('realmquantity').innerHTML=0
        document.getElementById('realmquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="realmswarden"></div><br>
        <font size='8' face='enchant'>
        $wardenName
        </font><br>
        $wardenDescript
        <br><br>
        <input type=button class="plusbutton" onclick="buttonplusrealm()" />
        <input type=button class="minusbutton" onclick="buttonminusrealm()" /><br><br>
                
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="realmquantity" class="itemquantity">0</span><br>
          <input id="realmquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="realmswarden">
          <input type="hidden" name="gold" value="15000">
          <input type="hidden" name="units" value="15">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id07" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id07').style.display='none' ; document.getElementById('metaquantity').innerHTML=0
        document.getElementById('metaquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="metamedallion"></div><br> 
        <font size='8' face='enchant'>
        $medallionName
        </font><br>
        $medallionDescript
        <br><br>
        <input type=button class="plusbutton" onclick="buttonplusmeta()" />
        <input type=button class="minusbutton" onclick="buttonminusmeta()" />
        
        <br><br>
                
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="metaquantity" class="itemquantity">0</span><br>
          <input id="metaquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="metamedallion">
          <input type="hidden" name="gold" value="25000">
          <input type="hidden" name="units" value="25">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id08" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id08').style.display='none' ; document.getElementById('ringquantity').innerHTML=0
        document.getElementById('ringquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="hushedring"></div><br>
        <font size='8' face='enchant'>
        $ringName
        </font><br>
        $ringDescript
        <br><br>
        <input type=button class="plusbutton" onclick="buttonplusring()" />
        <input type=button class="minusbutton" onclick="buttonminusring()" />
        
        <br><br>
                
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="ringquantity" class="itemquantity">0</span><br>
          <input id="ringquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="hushedring">
          <input type="hidden" name="gold" value="30000">
          <input type="hidden" name="units" value="30">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id09" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id09').style.display='none' ; document.getElementById('spellquantity').innerHTML=0
        document.getElementById('spellquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="spellwriting"></div><br>
        <font size='8' face='enchant'>
        $spellName
        </font><br>
        $spellDescript
        <br>
        <input type=button class="plusbutton" onclick="buttonpluswriting()" />
        <input type=button class="minusbutton" onclick="buttonminuswriting()" />
        
        <br><br>
                
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="spellquantity" class="itemquantity">0</span><br>
          <input id="spellquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="spellwriting">
          <input type="hidden" name="gold" value="40000">
          <input type="hidden" name="units" value="40">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id10" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id10').style.display='none' ; document.getElementById('eternalquantity').innerHTML=0
        document.getElementById('eternalquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="eternalslayer"></div><br>
        <font size='8' face='enchant'>
        $slayerName
        </font><br> 
        $slayerDescript
        <br><br>
        <input type=button class="plusbutton" onclick="buttonpluseternal()" />
        <input type=button class="minusbutton" onclick="buttonminuseternal()" />
        
        <br><br>
                
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="slayerquantity" class="itemquantity">0</span><br>
          <input id="slayerquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="eternalslayer">
          <input type="hidden" name="gold" value="60000">
          <input type="hidden" name="units" value="60">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id11" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id11').style.display='none' ; document.getElementById('starsquantity').innerHTML=0
        document.getElementById('starsquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="starfists"></div><br>
        <font size='8' face='enchant'>
        $starName
        </font><br> 
        $starDescript
        <br><br> 
        <input type=button class="plusbutton" onclick="buttonplusfalling()" />
        <input type=button class="minusbutton" onclick="buttonminusfalling()" />
        
        <br><br>
                
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="starsquantity" class="itemquantity">0</span><br>
          <input id="starsquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="fallingstars">
          <input type="hidden" name="gold" value="80000">
          <input type="hidden" name="units" value="80">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id12" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id12').style.display='none' ; document.getElementById('elixirquantity').innerHTML=0
        document.getElementById('elixirquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="dreamflower"></div><br> 
        <font size='8' face='enchant'>
        $dreamflowerName
        </font><br>
            $dreamflowerDescript
            <br>
            
        <input type=button class="plusbutton" onclick="buttonpluselixir()" />
        <input type=button class="minusbutton" onclick="buttonminuselixir()" />
        
        <br><br>
            
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="elixirquantity" class="itemquantity_special">0</span><br>
          <input id="elixirquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="dreamelixir">
          <input type="hidden" name="units" value="200">
          <input type="submit" name="action" class="buy_units_special" value="units" >
        </form>
    
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id13" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id13').style.display='none' ; document.getElementById('oathquantity').innerHTML=0
        document.getElementById('oathquantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="oath"></div><br>
        <font size='8' face='enchant'>
        $oathName
        </font><br> 
        $oathDescript
        <br><br> 
        <input type=button class="plusbutton" onclick="buttonplusoath()" />
        <input type=button class="minusbutton" onclick="buttonminusoath()" />
            
        <br><br>
                
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="oathquantity" class="itemquantity">0</span><br>
          <input id="oathquantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="oath">
          <input type="hidden" name="gold" value="80000">
          <input type="hidden" name="units" value="80">
          <input type="submit" name="action" class="buy_gold" value="gold">
          <input type="submit" name="action" class="buy_units" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

  <div id="id14" class="w3-modal">
    <div class="w3-modal-content">
      <div class="w3-container">
        <span onclick="document.getElementById('id14').style.display='none' ; document.getElementById('nonamequantity').innerHTML=0
        document.getElementById('nonamequantitytrue').value=0
        " class="w3-button w3-display-topright">&times;</span>
        <div id="noname"></div><br> 
        <font size='8' face='enchant'>
        $nonameName
        </font><br>
            $nonameDescript
            <br><br>
            
        <input type=button class="plusbutton" onclick="buttonplusnoname()" />
        <input type=button class="minusbutton" onclick="buttonminusnoname()" />
        
        <br><br>
                
        <form action="itembuy?lingua=$lingua" method="POST">
          <span id="nonamequantity" class="itemquantity_special">0</span><br>
          <input id="nonamequantitytrue" type="hidden" name="quantity" value="0">
          <input type="hidden" name="item" value="noname">
          <input type="hidden" name="units" value="500">
          <input type="submit" name="action" class="buy_units_special" value="units" >
        </form>
        
        <br><br><br>
      </div>
    </div>
  </div>

</font>
EOD;



$shop = <<<EOD
<div>
  <html>
    <head>
      <link rel="stylesheet" type="text/css" href="./style/style.css">
    </head>

    <body>

      $shopboxes

      <div id="shop" style="font-family: frutiger; font-size: 17px; color:white;">


          <!---HEALTH-->
          <div id="back" onclick="document.getElementById('id01').style.display='block'">
              <div id="health"></div>
              <div id="titletext">$potionName<br>
              <div id="gold"></div>
              <div id="goldprice">500</div>
              <div id="unit"></div>
              <div id="unitprice">1</div>
              </div>
          </div>  


          <!---MANA-->
          <div id="back" onclick="document.getElementById('id02').style.display='block'">
              <div id="mana"></div>
              <div id="titletext">$manaName<br>
              <div id="gold"></div>
              <div id="goldprice">500</div>
              <div id="unit"></div>
              <div id="unitprice">1</div>
              </div>
          </div>  


          <!---STAMINA-->
          <div id="back" onclick="document.getElementById('id03').style.display='block'">
              <div id="stamina"></div>
              <div id="titletext">$vigorName<br>
              <div id="gold"></div>
              <div id="goldprice">1500</div>
              <div id="unit"></div>
              <div id="unitprice">2</div>
              </div>
          </div>  
      
          <!---IMAGE DICES-->
          <div id="back" onclick="document.getElementById('id04').style.display='block'">
              <div id="dice"></div>
              <div id="titletext">$diceName<br>
              <div id="gold"></div>
              <div id="goldprice">8000</div>
              <div id="unit"></div>
              <div id="unitprice">8</div>
              </div>
          </div> 
          
          <!---REDSMITH-->
          <div id="back" onclick="document.getElementById('id05').style.display='block'">
              <div id="redsmith"></div>
              <div id="titletext">$hammerName<br>
              <div id="gold"></div>
              <div id="goldprice">10000</div>
              <div id="unit"></div>
              <div id="unitprice">10</div>
              </div>
          </div>  

          <!---REALMSWARDEN-->
          <div id="back" onclick="document.getElementById('id06').style.display='block'">
              <div id="realmswarden"></div>
              <div id="titletext">$wardenName<br>
              <div id="gold"></div>
              <div id="goldprice">15000</div>
              <div id="unit"></div>
              <div id="unitprice">15</div>
              </div>
          </div>  

          <!---METAMEDALLION-->
          <div id="back" onclick="document.getElementById('id07').style.display='block'">
              <div id="metamedallion"></div>
              <div id="titletext">$medallionName<br>
              <div id="gold"></div>
              <div id="goldprice">25000</div>
              <div id="unit"></div>
              <div id="unitprice">25</div>
              </div>
          </div>

          <!---HUSHEDRING-->
          <div id="back" onclick="document.getElementById('id08').style.display='block'">
              <div id="hushedring"></div>
              <div id="titletext">$ringName<br>
              <div id="gold"></div>
              <div id="goldprice">30000</div>
              <div id="unit"></div>
              <div id="unitprice">30</div>
              </div>
          </div> 

          <!---SPELLWRITING-->
          <div id="back" onclick="document.getElementById('id09').style.display='block'">
              <div id="spellwriting"></div>
              <div id="titletext">$spellName<br>
              <div id="gold"></div>
              <div id="goldprice">40000</div>
              <div id="unit"></div>
              <div id="unitprice">40</div>
              </div>
          </div>

          <!---ETERNAL SLAYER-->
          <div id="back" onclick="document.getElementById('id10').style.display='block'">
              <div id="eternalslayer"></div>
              <div id="titletext">$slayerName<br>
              <div id="gold"></div>
              <div id="goldprice">60000</div>
              <div id="unit"></div>
              <div id="unitprice">60</div>
              </div>
          </div> 

          <!---STARFISTS-->
          <div id="back" onclick="document.getElementById('id11').style.display='block'">
              <div id="starfists"></div>
              <div id="titletext">$starName<br>
              <div id="gold"></div>
              <div id="goldprice">80000</div>
              <div id="unit"></div>
              <div id="unitprice">80</div>
              </div>
          </div> 
  
          <!---OATH-->
          <div id="back" onclick="document.getElementById('id13').style.display='block'">
              <div id="oath"></div>
              <div id="titletext">$oathName<br>
              <div id="gold"></div>
              <div id="goldprice">80000</div>
              <div id="unit"></div>
              <div id="unitprice">80</div>
              </div>
          </div>

          <!---DREAMFLOWER ELIXIR-->
          <div id="backspecial" onclick="document.getElementById('id12').style.display='block'">
              <div id="dreamflower"></div>
              <div id="titletext">$dreamflowerName<br>
              <div id="goldprice"></div>
              <div id="unit"></div>
              <div id="unitprice">200</div>
              </div>
          </div> 
          
          <!---NO NAME-->
          <div id="backspecial" onclick="document.getElementById('id14').style.display='block'">
              <div id="noname"></div>
              <div id="titletext">$nonameName<br>
              <div id="goldprice"></div>
              <div id="unit"></div>
              <div id="unitprice">500</div>
              </div>
          </div>  
          
      </div>
    </body>
  </html>
</div>
EOD;


?>