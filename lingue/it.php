<?php
error_reporting(E_ALL & ~E_NOTICE);

///// MENU NAVIBAR /////

$HOME= "Home";
$FORUM="Forum";
$DOWNLOAD="Download";
$SHOP="Negozio";
$FAZIONI="Fazioni";
$DONATIONS="Donazioni";
$REGISTRAZIONE="Registrazione";

///// TITOLO GILDE /////

$guildtitle=<<<EOD
<div>
<html>
<div class="guild_it"></div>
</html>
</div> 
EOD; 


///// INTRO PRIMA VOLTA /////

$intro = "<br><br><font size='5' face='frutiger'>
<b>
Ciao e benvenuto nel nostro sito!
</b><br><br>
All'inizio della pagina troverai rispettivamente:
<br><br>
L'icona della tua fazione.
<br><br>
Un simbolo per gestire l'appartenenza a una gilda.
<br><br>
Il tuo nome con cui gestire e controllare il tuo profilo.
<br><br>
E infine la tua valuta corrente di oro e unità.
<br><br><br>
-><a href=index?lingua=$lingua><b>
Buona permanenza!
</b></a><-
</font><br><br>";

///// CHAT /////

$sendmessagechat = "<b>Invia</b>";

$closechat = "<b>Chiudi</b>";

$writemessage = "Scrivi un messaggio...";

$chatmessages = "Messaggi";

///// FORUM /////

$forumclosed= "<br><br><br><br><font size='5' face='frutiger'>Al momento il forum è chiuso, riprova più tardi.</font><br><br><br><br>" ;

$titolo = "<br><strong>Titolo</strong>:<br>\n";

$messaggio = "<strong>Messaggio</strong>:<br><br>\n";

$sendtopic = "Crea";

$topicanswerconfirm = "Rispondi";

$answertopic = "<b>Rispondi</b>";

$newtopic = " <b>Nuovo topic</b>";

$successtopic = "<br><br><br><br><font size='5' face='frutiger'>Il tuo topic è stato inviato con successo!</font><br><br><br><br>";

$successreply = "<br><br><br><br><font size='5' face='frutiger'>Il tuo messaggio è stato inviato con successo!</font><br><br><br><br>";

$enterforum = "Entra nel Forum";

$forumintro = "<br><br><br><br><font size='5' face='frutiger'>↑ Benvenuto/a nel forum! ↑<br><br>
Se avessi dei dubbi riguardo al gioco non esitare a farcelo sapere,<br> crea subito un nuovo topic e spiegaci il tuo problema.<br><br>
Se le risposte degli altri giocatori o di ospiti esterni<br> non dovessero soddisfarti, potrai sempre ricevere una risposta<br> da parte dei nostri admin.<br><br><br>
<b>Buona permanenza :)</b></font><br><br><br><br>";

///// REGISTRAZIONE /////

$cannotregister="<br><br><br><br><font size='5' face='frutiger'>Devi uscire dal tuo account per poterne registrare un altro.</font><br><br><br><br>";

$choosefaction='<font size="8" face="enchant">Scegli la tua Fazione</font><br><br>';

$havetodisconnect="<br><br><br><br><font size='5' face='frutiger'>Devi disconnetterti per poter creare un nuovo account.</font><br><br><br><br>";

$namealreadyused="<br><br><br><br><font size='5' face='frutiger'>Questo nome account è già stato utilizzato!</font><br><br><br><br>";

$registeroffline="<br><br><br><br><font size='5' face='frutiger'>Al momento non possono essere accettate registrazioni.<br><br>
                                                                Riprova tra qualche minuto.<br><br>
                                                                Ci scusiamo per il disagio.</font><br><br><br><br>";

$creationsuccess="<br><br><br><br><font size='5' face='frutiger'>Il tuo account è stato creato con successo!</font><br><br><br><br>";

$creationerror="<br><br><br><br><font size='5' face='frutiger'>Qualcosa è andato storto con la tua registrazione :(</font><br><br><br><br>";

$creationerrorname="<br><br><br><br><font size='5' face='frutiger'>Il tuo nome utente può essere composto solo da numeri e lettere semplici.</font><br><br><br><br>";

$creationerrorpassword="<br><br><br><br><font size='5' face='frutiger'>La tua password non corrisponde.</font><br><br><br><br>";

$CMS_REGISTERBOX ="<br><font size='8' face='enchant'>Si prega di compilare tutti i campi!</font><br></font>";

///// SERVERBOX INFO /////

$registered="Registrati<font size='4' face='comic sans ms'>:</font>";

$serverstatus="Stato del Server<font size='4' face='comic sans ms'>:</font>";

$servertime="Ora del Server<font size='3' face='frutiger'>:</font><b>";

$servertimeoffline="Ora del Server<font size='3' face='frutiger'>:</font><br><b>Offline</b><br>";

///// FAZIONI /////

$drarivaStory = "Drariva è la capitale di Dracoria, fulcro della più potente magia del mondo.
                <br><br>
                Questa enorme città si affaccia sul mare, ove grazie alle sue capienti navi, raccoglie conoscenza da ogni parte del globo.
                <br><br>
                Rinomata per il suo commercio, la cultura a Drariva si incentra sullo studio degli antichi draghi, considerati le più sapienti creature mai esistite.";

$providentiaStory = "Questa gigantesca megalopoli è la struttura
                        <br>
                         più tecnologicamente avanzata di tutto il pianeta. 
                        <br><br>
                        Essa consiste in un grande 'piatto' di forma perfettamente circolare,
                        <br> 
                        sospeso a decine di metri dal suolo da pilastri. 
                        <br><br>
                        Su questa struttura si trova la città vera e propria, suddivisa in otto settori, mentre al di sotto si trovano i bassifondi,
                        <br>
                         che un tempo costituivano la vera città.
                         <br><br>
                         Un tempo gli otto settori erano città separate, 
                         <br>
                         ma in seguito furono riunite e i loro nomi furono col tempo dimenticati."; 

///// SHOP /////

$noquantity="<br><br><br><br><font size='5' face='frutiger'>Non hai selezionato una quantità adeguata!</font><br><br><br><br>";

$nologinshop="<br><br><br><br><font size='5' face='frutiger'>Non hai effettuato il login!</font><br><br><br><br>";

$cannotshop="<br><br><br><br><font size='5' face='frutiger'>Siamo spiacenti, ma l'acquisto di oggetti è riservato ai soli registrati.<br><br>
             Provvedi subito <a href='register?lingua=$lingua' onclick='document.getElementById('body_centro').style.opacity='0'>qui</a>.</font><br>
             <div class='shopclosed'></div><br><br>
             ";

$notenoughunits="<br><br><br><br><font size='5' face='frutiger'>Non hai abbastanza unità :(</font><br><br><br><br>";

$notenoughgold="<br><br><br><br><font size='5' face='frutiger'>Non hai abbastanza oro :(</font><br><br><br><br>";

$buysuccess="<br><br><br><br><font size='5' face='frutiger'>Il tuo acquisto è stato effettuato con successo!</font><br><br><br><br>";

$buyfailure="<br><br><br><br><font size='5' face='frutiger'>Qualcosa è andato storto con il tuo acquisto :(</font><br><br><br><br>";

//// ITEMS NAME ////

$potionName = "Cura Superiore";

$manaName = "Mana Superiore";

$vigorName = "Vigore Migliorato";

$diceName = "Dadi dell'immagine";

$hammerName = "Martello Scarlatto";

$wardenName= "Guardia dei Regni";

$medallionName= "Meta Medaglione";

$ringName = "Anello del Silenzio";

$spellName = "Scrivincanta";

$slayerName = "Fendente Eterno";

$starName = "Stelle Cadenti";

$oathName = "Giuramento";

$dreamflowerName = "Elisir dei Sogni";

$nonameName = "Senza Nome";

//// ITEMS DESCRIPTION ////

$potionDescript = "Questa fantastica pozione restituisce al proprio bevitore
                <br>
                un'incredibile quantità di punti ferita, rendendolo sempre
                <br>
                pronto ad affrontare i pericoli più temibili.";


$manaDescript = "Scontri che prosciugano il tuo mana non saranno più un
                <br>
                problema grazie alla nostra pozione di mana speciale.";

$vigorDescript = "Riscopri la forza di un drago e schiaccia i tuoi nemici.";

$diceDescript = "Hai bisogno di un'esca convincente?
                <br>
                Allora i nostri dadi fanno al caso tuo!";

$hammerDescript = "Migliora in tutta sicurezza il tuo equipaggamento,
                <br>
                con l'utilizzo del nostro martello speciale non hai più il rischio di romperlo.";

$wardenDescript = "Si dice che le guardie dell'antico regno dei giganti usassero
                    <br>
                    asce esattamanete come questa.";

$medallionDescript= "Attraverso l'aiuto degli spiriti contenuti in questo medaglione
                    <br>
                    avrai un notevole aumento di rigenerazione del mana!";

$ringDescript = "Semina i tuoi nemici, o spiali grazie al nostro speciale anello del silenzio!";

$spellDescript = "Questa particolare fascia per il braccio infusa di nano-fate ti permette
                <br>
                di lanciare i tuoi incantesimi senza consumare mana per un tempo duraturo!";

$slayerDescript = "Nemmeno il diamante può resistere al micidiale fendente di questa spada straordinaria!";

$starDescript = "Sprigiona la straordinaria forza cosmica dalle tue nocche sulla faccia dei tuoi nemici!";

$oathDescript = "Il ricordo di un'arcaica promessa ancora vive in questo scudo.";

$dreamflowerDescript = "Un'antica pozione considerata leggendaria per i suoi
                        <br>
                        miracolosi effetti sovrannaturali.
                        <br>
                        Si dice che basti berla per essere avvolti da un'incredibile fortuna.";

$nonameDescript = "...";


///// DONAZIONI /////

$donationIntro = "Di seguito trovi i nostri pulsanti per le donazioni:";
$donationReminder1 = 'Ricordati successivamente di mandare una mail con oggetto "Donations" e il tuo nome utente a:';
$donationReminder2 = 'È obbligatorio che la mail della donazione e quella di conferma corrispondano.';
$donationThanks = "Grazie :)";


///// LOGIN /////

$alreadylogin="<font size='5' face='frutiger'>
<br><br><br>
Sei gia loggato!
<br><br><br>
</font>
";

$alreadydisconnected="<br><br><br><font size='5' face='frutiger'>
Sei già stato disconnesso!<br><br>
<br></font>";

$disconnect="<br><br><br><font size='5' face='frutiger'>
Ti sei disconnesso correttamente.<br><br>
A presto!
<br><br><br></font>";

$correctlogin="<br><br><br><font size='5' face='frutiger'>
Hai effettuato l'accesso correttamente.
<br><br></font>";

$wronglogin="<br><br><br><font size='5' face='frutiger'>
Hai inserito un nome utente o una password sbagliati!<br><br>Per favore riprova!
<br><br><br></font>";


///// HOME //////

$CMS_NEWS =
"

<br><br>
<font size='6' face='enchant'><b>Grazie per aver scelto di giocare con noi<font size='5' face='comic sans ms'>!</font></b><br></font>

<font size='3' face='frutiger'>
<b>Per giocare devi prima <a style= 'display: inline; color: black;' href='register?lingua=$lingua'>registrarti</a>.<br>
Dopo la registrazione potrai scaricare e giocare attraverso il client!</b><br><br><br>
</font>                

<font size='6' face='enchant'><b>Vuoi sostenerci<font size='5' face='comic sans ms'>?</font></b><br></font>

<font size='3' face='frutiger'><b>
Tramite la donazione puoi fornire dei piccoli contributi per il nostro server.<br>
Come ringraziamento riceverai da noi della valuta di gioco,<br>che potrai utilizzare per acquistare oggetti ingame nel nostro shop.<br><br><br>
</font> 

<font size='6' face='enchant'><b>Vuoi segnalare un giocatore<font size='5' face='comic sans ms'>?</font></b><br></font>

<font size='3' face='frutiger'>
Anche questo non è un problema.<br> Basta andare sul nostro forum e creare un argomento apposito.<br><br><br>
</font> 

<font size='6' face='enchant'><b>Hai bisogno di aiuto<font size='5' face='comic sans ms'>?</font></b><br></font>

<font size='3' face='frutiger'>
Se hai bisogno di aiuto prova nel nostro forum.<br>
Se avessi domande a cui la community del gioco non potesse rispondere, <br>puoi anche contattare direttamente il nostro team di supporto.<br>
Puoi trovare i nostri contatti Facebook o Instagram qui a lato.<br><br>
 
<font size='5'><h4>Ti auguriamo buon divertimento! :)</h4></font>
</font>
<font size='8' face='enchant'><h3>Il Team</h3></font>   
 
</font>
";


///// DOWNLOAD /////

$CMS_DOWNLOAD = 
    "
    <font size='5' face='frutiger'>
    <br>
    Al fine di garantire un corretto comportamento del gioco, dovresti prima notare le seguenti informazioni di sistema:<br>

    <h3>Requisiti</h3>
    <b>OS:</b> Windows 7/8/10<br>
    <b>CPU:</b> i5 3.0GHz<br>
    <b>RAM:</b> 8G<br>
    <b>VGA:</b> Geforce GTX 1060 6GB<br>
    <b>DirectX:</b> DirectX 11/12<br>
    <b>HDD/SSD:</b> 20GB<br><br>
    
    </font>
    ";


///// TITOLO GILDE /////

$warlock = "Stregoni Supremi";

$guardian = "Guardiani del Caos";

$seeker = "Cacciatori del Tesoro";

$wise = "Antichi Sapienti";

$paladin = "Paladini dell'Eterno";

$members = "Membri della gilda";

$joinGuild = "Entra nella gilda";

$exitGuild = "Esci dalla tua gilda";

$selectRole = "Seleziona un Ruolo";

$attackerRole = "Corpo a Corpo";

$defenderRole = "Difensore";

$magicRole = "Offensiva Magica";

$healerRole = "Guaritore";

//// PROFILO ////

$faction = "Fazione";

$guild = "Gilda";

$guildRole = "Ruolo";

$inventory = "Inventario";

$none = "Nessuna";

?>