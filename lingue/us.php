<?php
error_reporting(E_ALL & ~E_NOTICE);

///// MENU NAVIBAR /////

$HOME= "Home";
$FORUM="Forum";
$DOWNLOAD="Download";
$SHOP="Shop";
$FAZIONI="Factions";
$DONATIONS="Donations";
$REGISTRAZIONE="Registration";

///// TITOLO GILDE /////

$guildtitle=<<<EOD
<div>
<html>
<div class="guild_us"></div> 
</html>
</div> 
EOD;

///// INTRO PRIMA VOLTA /////

$intro = "<br><br><font size='5' face='frutiger'>
<b>
Hi and welcome to our site!
</b><br><br>
At the beginnig of the page you'll find:
<br><br>
Your faction icon.
<br><br>
A symbol to manage your guild membership.
<br><br>
Your name, that you can click to manage and see your profile.
<br><br>
Your current value of gold and units.
<br><br><br>
-><a href=index?lingua=$lingua><b>
Enjoy your stay!
</b></a><-
</font><br><br>";

///// CHAT /////

$sendmessagechat = "<b>Send</b>";

$closechat = "<b>Close</b>";

$writemessage = "Type a message...";

$chatmessages = "Messages";

///// FORUM /////

$forumclosed= "<br><br><br><br><font size='5' face='frutiger'>The Forum is closed at the moment, try again later.</font><br><br><br><br>" ;

$titolo = "<br><strong>Title</strong>:<br>\n";

$messaggio = "<strong>Message</strong>:<br><br>\n";

$sendtopic = "Create";

$topicanswerconfirm = "Reply";

$answertopic = "<b>Reply</b>";

$newtopic = " <b>New Topic</b>";

$successtopic = "<br><br><br><br><font size='5' face='frutiger'>Your topic has been sent!</font><br><br><br><br>";

$successreply = "<br><br><br><br><font size='5' face='frutiger'>Your message has been sent!</font><br><br><br><br>";

$enterforum = "Enter Forum";

$forumintro = "<br><br><br><br><font size='5' face='frutiger'>↑ Welcome to our Forum! ↑<br><br>
If you have any doubt reguarding the game don't hesitate and<br> let us know, create a new topic and explain your problem.<br><br>
If other players' answers doesn't suffice,<br> our admins team will be glad to help you.<br><br><br>
<b>Enjoy your stay :)</b></font><br><br><br><br>";

///// REGISTRAZIONE /////

$cannotregister="<br><br><br><br><font size='5' face='frutiger'>You have to log off to register a new account.</font><br><br><br><br>";

$choosefaction='<font size="8" face="enchant">Choose your Faction</font><br><br>';

$havetodisconnect="<br><br><br><br><font size='5' face='frutiger'>Ypu have to disconnect first.</font><br><br><br><br>";

$namealreadyused="<br><br><br><br><font size='5' face='frutiger'>This username has already been taken!</font><br><br><br><br>";

$registeroffline="<br><br><br><br><font size='5' face='frutiger'>We're not accepting any new registration at the moment.<br><br>
                                                                Try again later.<br><br>
                                                                We're sorry for the inconvenience.</font><br><br><br><br>";

$creationsuccess="<br><br><br><br><font size='5' face='frutiger'>Your account was successfully created!</font><br><br><br><br>";

$creationerror="<br><br><br><br><font size='5' face='frutiger'>Something went wrong with your registration :(</font><br><br><br><br>";

$creationerrorname="<br><br><br><br><font size='5' face='frutiger'>Your username con only contain simple letters and numbers.</font><br><br><br><br>";

$creationerrorpassword="<br><br><br><br><font size='5' face='frutiger'>Your password doesn't match.</font><br><br><br><br>";

$CMS_REGISTERBOX ="<br><font size='8' face='enchant'>Please fill all the blanks!</font><br></font>";

///// SERVERBOX INFO /////

$registered="Registered<font size='4' face='comic sans ms'>:</font>";

$serverstatus="Server Status<font size='4' face='comic sans ms'>:</font>";

$servertime="Server Time<font size='3' face='frutiger'>:</font><b>";

$servertimeoffline="Server Time<font size='3' face='frutiger'>:</font><br><b>Offline</b><br>";

///// FAZIONI //////

$drarivaStory = "Drariva is Dracoria's capital, center of the most powerful magic in the world.
                <br><br>
                This huge city is located near the sea, where thanks to its giant ships, gather knowledge all around the globe.
                <br><br>
                Renowed for its trade, the culture is focused on the study of the ancient dragons, believed to be the most powerful creatures ever existed.";

$providentiaStory = "Providentia is the most technologically advanced city on the planet. 
                    <br><br>
                    Its large circular structure is supported above the ground by several pillars and a central one, with various other supports around the city. 
                    <br><br>
                    On top of the structure lies the city proper, divided into eight sectors, while underneath it lie the slums for the city's unfortunate and downtrodden. 
                    <br><br>
                    Providentia was once eight individual towns, but their names have been forgotten in favor of referring to the different areas as sectors.";

///// SHOP /////

$noquantity="<br><br><br><br><font size='5' face='frutiger'>You didn't select a valid quantity!</font><br><br><br><br>";

$nologinshop="<br><br><br><br><font size='5' face='frutiger'>You're not signed in!</font><br><br><br><br>";

$cannotshop="<br><br><br><br><font size='5' face='frutiger'>We're sorry, but purchasing is reserved to players.<br><br>
             Make it up right <a href='register?lingua=$lingua' onclick='document.getElementById('body_centro').style.opacity='0'>now</a>.</font><br>
             <div class='shopclosed'></div><br><br>
             ";

$notenoughunits="<br><br><br><br><font size='5' face='frutiger'>You don't have enough units :(</font><br><br><br><br>";

$notenoughgold="<br><br><br><br><font size='5' face='frutiger'>You don't have enough gold :(</font><br><br><br><br>";

$buysuccess="<br><br><br><br><font size='5' face='frutiger'>Your purchase was a success!</font><br><br><br><br>";

$buyfailure="<br><br><br><br><font size='5' face='frutiger'>Something went wrong with your purchase :(</font><br><br><br><br>";

//// ITEMS NAME ////

$potionName = "Superior Health Potion";

$manaName = "Superior Mana Potion";

$vigorName = "Improved Vigor Potion";

$diceName = "Image Dice";

$hammerName = "Red Hammer";

$wardenName= "Realms Warden";

$medallionName= "Meta Medallion";

$ringName = "Hushed Ring";

$spellName = "Spellwriting";

$slayerName = "Eternal Slayer";

$starName = "Falling Stars";

$oathName = "Oath";

$dreamflowerName = "Dream Elixir";

$nonameName = "No Name";

//// ITEMS DESCRIPTION ////

$potionDescript = "This fantastic potion restores an incredible amount
                    <br>
                    of health points, keeping you always ready to face
                    <br>
                    the most terrible situations.";


$manaDescript = "Fights that consumes your mana won't be a problem
                <br>
                anymore thanks to our special potion";

$vigorDescript = "Feel the true power of a dragon and wreck your enemies.";

$diceDescript = "Do you need a convincing bait?
                <br>
                Then our dices will come in handy!";

$hammerDescript = "Improve your equipment in total security,
                    <br>
                    with our special hammer you won't risk to break it anymore.";

$wardenDescript = "It is said that the guards of the ancient kingdom of the giants
                    <br>
                    had axes exactly like this one.";

$medallionDescript= "Through the help of the spirits contained in this medallion
                    <br>
                    you'll have a major mana regeneration rate!";

$ringDescript = "Escape your enemies, or spy on them with our amazing hushed ring!";

$spellDescript = "This particular armband infused with nano-faires allows you
                    <br>
                    to cast your spells without consuming your mana for a limited time!";

$slayerDescript = "Not even a diamond can resist the blow from this deadly blade!";

$starDescript = "Unleash the tremendous cosmic force from your knuckles on the face of your enemies!";

$oathDescript = "The memory of an ancient promise still lives inside this shield.";

$dreamflowerDescript = "An ancient potion considered legendary for its
                        <br>
                        miracolous and supernatural effects.
                        <br>
                        It is foretold that you only need to drink it to be incredibly lucky.";

$nonameDescript = "...";


///// DONAZIONI /////

$donationIntro = "Below you will find our donation buttons:";
$donationReminder1 = 'The email used for the donation and the one used for the confirm must be the same.';
$donationReminder2 = 'The email used for the donation and the one used for the confirm must be the same.';
$donationThanks = "Thank you :)";


///// LOGIN /////

$alreadylogin="<font size='5' face='frutiger'>
<br><br><br>
You're already signed in!
<br><br><br>
</font>
";

$alreadydisconnected="<br><br><br><font size='5' face='frutiger'>
You have already been disconnected!<br><br>
<br></font>";

$disconnect="<br><br><br><font size='5' face='frutiger'>
You have been successfully disconnected.<br><br>
See you soon!
<br><br><br></font>";

$correctlogin="<br><br><br><font size='5' face='frutiger'>
You have been successfully signed in.
<br><br></font>";

$wronglogin="<br><br><br><font size='5' face='frutiger'>
You insert a wrong username or password!<br><br>Please try again!
<br><br><br></font>";

///// HOME //////

$CMS_NEWS =
"

<br><br>
<font size='6' face='enchant'><b>Thanks for playing with us<font size='5' face='comic sans ms'>!</font></b><br></font>

<font size='3' face='frutiger'>
<b>To play you first have to  <a style= 'display: inline; color: black;' href='register?lingua=".$lingua."'>register</a>.<br>
After that you will be able to download and play with our client!</b><br><br><br>
</font>                

<font size='6' face='enchant'><b>Want to support us<font size='5' face='comic sans ms'>?</font></b><br></font>

<font size='3' face='frutiger'><b>
Through small donations you can keep our server running.<br>
As a reward you will receive some ingame value,<br>which you could use to purchase items in our shop.<br><br><br>
</font> 

<font size='6' face='enchant'><b>Want to flag a player<font size='5' face='comic sans ms'>?</font></b><br></font>

<font size='3' face='frutiger'>
No problem.<br> Just go to our forum and make a topic about that.<br><br><br>
</font> 

<font size='6' face='enchant'><b>Need some help<font size='5' face='comic sans ms'>?</font></b><br></font>

<font size='3' face='frutiger'>
If you need help try with our forum.<br>
If you have questions none of the players can answer, <br>you can directly contact our support team.<br>
You can find our Facebook and Instagram right to the left.<br><br>
 
<font size='5'><h4>Have Fun! :)</h4></font>
</font>
<font size='8' face='enchant'><h3>The Team</h3></font>   
 
</font>
";


///// DOWNLOAD /////

$CMS_DOWNLOAD = 
    "
    <font size='5' face='frutiger'>
    <br>
    To ensure a correct behaviour of the game,<br> please be sure to check the requirements below:<br>

    <h3>Requirements</h3>
    <b>OS:</b> Windows 7/8/10<br>
    <b>CPU:</b> i5 3.0GHz<br>
    <b>RAM:</b> 8G<br>
    <b>VGA:</b> Geforce GTX 1060 6GB<br>
    <b>DirectX:</b> DirectX 11/12<br>
    <b>HDD/SSD:</b> 20GB<br><br>
    
    </font>
    ";


///// TITOLO GILDE /////

$warlock = "The Supreme Sorcerer";

$guardian = "Chaos Guardians";

$seeker = "Treasure Hunters";

$wise = "The Ancient Wises";

$paladin = "Paladins of the Eternal";

$members = "Guild's members";

$joinGuild = "Join this guild";

$exitGuild = "Leave your guild";

$selectRole = "Select a Role";

$attackerRole = "Attacker";

$defenderRole = "Defender";

$magicRole = "Magic Offense";

$healerRole = "Healer";

//// PROFILO ////

$faction = "Faction";

$guild = "Guild";

$guildRole = "Role";

$inventory = "Inventory";

$none = "None";

?>